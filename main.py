from random import randint
from typing import List, Optional
import boto3
import uuid
from fastapi import FastAPI

from concurrent.futures import ThreadPoolExecutor

aws_access_key_id = 'AKIA47CRVHZFHPEWCE4S'
aws_secret_access_key = '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'
queue_url = 'https://sqs.us-east-1.amazonaws.com/891376975434/colas2.fifo'
sqs = boto3.client(
    'sqs',
    region_name='us-east-1',
    aws_access_key_id=aws_access_key_id,
    aws_secret_access_key=aws_secret_access_key
)
app = FastAPI()

def encolar(cantidad_mensajes: int):
    mensajes = list()
    for i in range(0, cantidad_mensajes):
        response = sqs.send_message(
            QueueUrl=queue_url,
            MessageBody=f"{randint(0, cantidad_mensajes)}",
            MessageGroupId='pagos',
            MessageDeduplicationId=str(uuid.uuid4())
        )
        mensajes.append(response['MessageId'])
        print(f"Enviando Transaction {i} - {response['MessageId']}")
    return mensajes

def desencolar_todos_ordenados():
    mensajes_desencolados = list()
    while True:
        response = sqs.receive_message(
            QueueUrl=queue_url,
            AttributeNames=[
                'All'
            ],
            MessageAttributeNames=[
                'All'
            ],
            MaxNumberOfMessages=10,  # You can receive up to 10 messages at a time
            VisibilityTimeout=30,
            WaitTimeSeconds=0
        )
        if response.get('Messages'):
            for message in response['Messages']:
                print(f"Mensaje recibido: {message['Body']}")
                sqs.delete_message(
                    QueueUrl=queue_url,
                    ReceiptHandle=message['ReceiptHandle']
                )
                mensajes_desencolados.append(int(message['Body']))  # Convertimos el mensaje a entero y lo añadimos
        else:
            print("No se encontraron más mensajes en la cola.")
            break  # Salir del bucle si no hay más mensajes en la cola
    # Ordenamos los mensajes desencolados antes de retornarlos
    mensajes_desencolados.sort()
    return mensajes_desencolados

class AVLNode:
    def __init__(self, key):
        self.key = key
        self.left = None
        self.right = None
        self.height = 1

class AVLTree:
    def insert(self, root: Optional[AVLNode], key: int) -> AVLNode:
        if not root:
            return AVLNode(key)
        elif key < root.key:
            root.left = self.insert(root.left, key)
        elif key > root.key:
            root.right = self.insert(root.right, key)
        else:
            return root
        root.height = 1 + max(self.get_height(root.left), self.get_height(root.right))
        balance = self.get_balance(root)
        if balance > 1 and key < root.left.key:
            return self.right_rotate(root)
        if balance < -1 and key > root.right.key:
            return self.left_rotate(root)
        if balance > 1 and key > root.left.key:
            root.left = self.left_rotate(root.left)
            return self.right_rotate(root)
        if balance < -1 and key < root.right.key:
            root.right = self.right_rotate(root.right)
            return self.left_rotate(root)
        return root
    def left_rotate(self, z: AVLNode) -> AVLNode:
        y = z.right
        T2 = y.left
        y.left = z
        z.right = T2
        z.height = 1 + max(self.get_height(z.left), self.get_height(z.right))
        y.height = 1 + max(self.get_height(y.left), self.get_height(y.right))
        return y
    def right_rotate(self, z: AVLNode) -> AVLNode:
        y = z.left
        T3 = y.right
        y.right = z
        z.left = T3
        z.height = 1 + max(self.get_height(z.left), self.get_height(z.right))
        y.height = 1 + max(self.get_height(y.left), self.get_height(y.right))
        return y
    def get_height(self, root: Optional[AVLNode]) -> int:
        if not root:
            return 0
        return root.height
    def get_balance(self, root: Optional[AVLNode]) -> int:
        if not root:
            return 0
        return self.get_height(root.left) - self.get_height(root.right)
    def pre_order(self, root: Optional[AVLNode]):
        res = []
        if root:
            res.append(root.key)
            res = res + self.pre_order(root.left)
            res = res + self.pre_order(root.right)
        return res

def crear_arbol_binario(mensajes: List[int]):
    arbol = AVLTree()
    root = None
    for mensaje in mensajes:
        root = arbol.insert(root, mensaje)
    return root, arbol

def graficar_arbol(root: Optional[AVLNode], level=0, pref="Root:"):
    if root is not None:
        print(" " * (level * 4) + pref + str(root.key))
        if root.left:
            graficar_arbol(root.left, level + 1, "L---")
        if root.right:
            graficar_arbol(root.right, level + 1, "R---")

@app.post("/encolar")
def sqs_encolar(parametros: dict):
    mensajes = encolar(parametros["cantidad_mensajes"])
    return {
        "Mensajes encolados": mensajes,
        "cantidad de encolados": len(mensajes)
    }

@app.post("/desencolar")
def sqs_desencolar():
    mensajes = desencolar_todos_ordenados()
    # Construimos el árbol binario de búsqueda
    root, arbol = crear_arbol_binario(mensajes)
    print("Árbol AVL generado (Pre-Order):", arbol.pre_order(root))
    graficar_arbol(root)
    return {
        "desencolados_ordenados": mensajes,
        "cantidad de desencolados": len(mensajes),
        "arbol_generado": arbol.pre_order(root)
    }